
/**
 * Add the viewportOffset function jQuery.
 */
(function($){
    '$:nomunge'; // Used by YUI compressor.

    var win = $(window);

    $.fn.viewportOffset = function() {
        var offset = $(this).offset();

        if(offset==null) return null;

        return {
            left: offset.left - win.scrollLeft(),
            top: offset.top - win.scrollTop()
        };
    };

})(jQuery);

var removeElements = function(text, selector) {
    var wrapped = jQuery("<div>" + text + "</div>");
    wrapped.find(selector).remove();
    return wrapped.html();
};

(function ($) {
    Drupal.behaviors.gulp_asset = {
        attach: function (context, settings) {


            function getScrollBarWidth(){
                if($(document).height() > $(window).height()){
                    $('body').append('<div id="fakescrollbar" style="width:50px;height:50px;overflow:hidden;position:absolute;top:-200px;left:-200px;"></div>');
                    fakeScrollBar = $('#fakescrollbar');
                    fakeScrollBar.append('<div style="height:100px;">&nbsp;</div>');
                    var w1 = fakeScrollBar.find('div').innerWidth();
                    fakeScrollBar.css('overflow-y', 'scroll');
                    var w2 = $('#fakescrollbar').find('div').html('html is required to init new width.').innerWidth();
                    fakeScrollBar.remove();
                    return (w1-w2);
                }
                return 0;
            }

            //Configure colorbox call back to resize with custom dimensions
            $.colorbox.settings.onLoad = function() {
                colorboxResize();
            };

            //Customize colorbox dimensions
            var colorboxResize = function(resize) {
                var width = "90%";
                var height = "90%";

                if($(window).width() > 960) { width = "860" }
                if($(window).height() > 700) { height = "630" }

                $.colorbox.settings.height = height;
                $.colorbox.settings.width = width;

                //if window is resized while lightbox open
                if(resize) {
                    $.colorbox.resize({
                        'height': height,
                        'width': width
                    });
                }
            }

            /**
                     *  Make Menus inside navigation section Bootstrap capable.
                     */
            $('header#navbar nav[role=navigation] ul').addClass('navbar-nav');



            var $topNavLinks = $('.top-nav ul.menu li a, .bottom-nav ul.menu li a');

            $topNavLinks.each(function(){

                var text = $(this).text();
                var len = text.length;
                var endPoint = text.lastIndexOf('.');

                if(endPoint != -1) {
                    endPoint += 1;
                    var newText = text.substring(0, endPoint) + "<span class='last2'>" +
                        text.substring(endPoint, len) + "</span>";
                    $(this).html(newText);
                }
            });


            var $langSwitchers = $('.language-switcher-nav');

            $langSwitchers.each(function(){
                var $langParentUserMenu = $(this)
                    .closest('.language-switcher-nav')
                    .siblings('.user-nav-menu').find('ul');


                var $link = $(this).find('ul li a');
                $link.each(function(){
                    $(this).text($(this).attr('lang')).addClass('nav-button');
                    $(this).closest('li:not(.block-configure)').detach().appendTo($langParentUserMenu)
                });

            });

            $langSwitchers.remove();


            /**
             * Resize the Home Page Top Section to cover the entire screen size
             * */
            function HeightCalc(bottomSpace) {
                var $view = $('.slides-region');
                var $offset = $view.viewportOffset();
                if ($offset == null)
                    return;

                var viewTop = $offset.top;
                var pageHeight = $(window).height();
                var ViewHeight = pageHeight - viewTop;

                $view.css('height', ViewHeight - bottomSpace + 'px')
            }



            function doResizing(extra) {

                var topScroll = $(window).scrollTop();

                if( topScroll == 0){

                    var bottomPadding = $(".unique-collections-region.top").outerHeight(true);
                    var wWidth = $(window).width() + getScrollBarWidth();

                    if( wWidth < 768 ){
                        HeightCalc(0 - extra);

                    }else if (wWidth >= 768 && wWidth <= 1024) {
                        HeightCalc(bottomPadding - extra);
                    }
                    else{
                        HeightCalc(bottomPadding - extra);
                    }
                }
            }


            $(window).resize(function() {
                doResizing(0);
                colorboxResize(true);
            });


            //var fallbackImage = settings.gulp_asset.imagesPath + '/product-placeholder.jpg';
            //
            //$('img').on('error', function() {
            //    $(this).attr('src', fallbackImage);
            //});

            /**
             * Remove unwanted text from facet labels
             */
            $("ul[class*='facetapi'] li label[for*='checkbox']").each(function(){

                $(this).addClass('styled-label');
                var $anchor = $(this).siblings('a');

                var newText = removeElements($anchor.html(), "span");

                if( newText.indexOf("(-)") != -1 ) {

                    newText = $(this).text().replace('Apply', '');
                    newText = newText.replace('filter', '');
                    newText = newText.replace('Remove', '');
                }

                $(this).text(newText);
                $anchor.addClass('styled-anchor');

                var $siblingInput = $(this).next('input');
                $siblingInput.addClass('styled-input');
                $(this).insertAfter($siblingInput);

                $(this).closest('li').contents().filter(function(){
                    return this.nodeType === 3;
                }).remove();


            })

            var wWidth = $(window).width() + getScrollBarWidth();

            if(wWidth <= 768){
                doResizing(15);
            }
            else if(wWidth >= 769 && wWidth <= 1024) {
                doResizing(15);
            }
            else {
                doResizing(0);
            }


            /**
             *  Makes the distillery image a background image.
             */

            var $distImage = $('.slides-region .views-field-field-distillery-image');
            if ($distImage.length > 0) {
                var imgPath = $distImage.find('img').attr('src');

                $distImage.closest('section.block').attr('data-image-changed', 'true').addClass('inverted')
                    .css('background', 'url('+ imgPath + ')');

                $distImage.remove();

            }


            /**
             * Style the distillery filters
             */
            var $viewProductSearch = $('.view-products-search');

            if($viewProductSearch.length > 0){

                var modeClass = 'grid-mode-view';
                var $items= $viewProductSearch.find('.view-header .total-items');
                var $modes= $viewProductSearch.find('.view-header .view-mode-buttons');
                var $filters = $viewProductSearch.find('#edit-sort-bef-combine-wrapper');
                var $modeText = $modes.find('.view-mode-text');
                var isEmpty = $viewProductSearch.find('.view-empty').length;

                var icons = [
                    'fa fa-sort-alpha-asc',
                    'fa fa-sort-alpha-desc',
                    'fa fa-sort-numeric-asc',
                    'fa fa-sort-numeric-desc',
                    'fa fa-sort-amount-asc',
                    'fa fa-sort-amount-desc'
                ];


                if(isEmpty){
                    $filters.remove();
                    return;

                }


                $viewProductSearch.addClass(modeClass);

                $filters.insertAfter($items);
                $filters.find('label').remove();
                $filters.find('.form-group .form-item div')
                    .each(function(el, i){
                        $(this).find('a').html("<i class='" + icons[el] + "'></i>");
                    });

                $filters.append("<div class='filters-legend'>Alphabetic/Price</div>");

                $modes.find('.buttons .switch-btn').click(function(){
                    var newClass= $(this).attr('data-view-class');
                    $viewProductSearch.removeClass(modeClass).addClass(newClass);
                    modeClass = newClass;

                    $(this).closest('.buttons').find('span').removeClass('active');
                    $(this).addClass('active');

                });

                $modes.find('.buttons .switch-btn').hover(
                    function(){
                        $modeText.html($(this).attr('data-message'));
                    },

                    function(){
                        $modeText.html('');
                    }
                );

            }

            /**
                     *   Implements Fast Live Search on Facet Blocks
                     */

            var $facetBloks = [
                '#facetapi-facet-search-apiproduct-display-block-field-landname',
                '#facetapi-facet-search-apiproduct-display-block-field-distilleryname',
                '#facetapi-facet-search-apiproduct-display-block-field-land'
            ];



            $.each($facetBloks, function(i, e){

                var $fblock = $(e);

                if($fblock.hasClass('fast-list-' + i)) {
                    return;
                }

                $fblock.addClass('fast-list-' + i);

                var $inputSearch = $("<div id='fast-search-" + i + "' class='fast-live-search'><input id='fast-input-" + i + "' type='text'/></div>")

                $inputSearch.insertBefore($fblock);
                $('#fast-input-' + i).fastLiveFilter('.fast-list-' + i);

            });

            $('.sidebar-inner section[class*=block-facetapi] >ul').css('max-height', '120px')
                .addClass('scrollbar-macosx')
                .mCustomScrollbar();



            $('a[href*=#]:not([href=#])').click(function() {
                var anchor = $(this);
                if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                    if (target.length) {
                        $('html,body').animate({
                            scrollTop: target.offset().top - 180
                        }, 1000);
                        console.log(anchor);
                        return false;
                    }
                }
            });

        }
    }
})(jQuery);
